#include "wsports.h"
#include "websocket.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>

#include <dirent.h>

#include <assert.h>
//Copy of the config struct.
static ws_config config_srv;

//Child process list
struct s_children;
struct s_children {
  pid_t pid;
  struct s_children *next;
};
typedef struct s_children t_children;
//Children list instance.
static t_children children;

static int is_process_alive(pid_t pid){
  //@bug NOT PORTABLE! Linux-only
  char procpath[128];
  memset(procpath, 0, sizeof(procpath));
  snprintf(procpath, sizeof(procpath) - 1, "/proc/%d/", pid);

  /* Opendir approach */
  DIR* dir_entry = opendir(procpath);
  logmsg(WS_LOG_AUTISTIC, procpath);
  if(dir_entry == NULL){
    logmsg(WS_LOG_AUTISTIC, "/proc entry NOT FOUND or invalid (process dead)");
    return 0;
  }
  else{
    logmsg(WS_LOG_AUTISTIC, "/proc entry FOUND (process alive)");
    closedir(dir_entry);
    return 1;
  }
}

static void child_cleanup(t_children *c_list){
  assert(c_list);
  do{
    //If next is a valid pointer
    if(c_list->next){
      //Check if it is alive
      if (!is_process_alive(c_list->next->pid))
      {
        {
          char buff[128];
          memset(buff, 0, sizeof(buff));
          snprintf(buff, sizeof(buff) - 1, "Removing dead process %d from child process list", c_list->next->pid);
          logmsg(WS_LOG_AUTISTIC, buff);
        }
        t_children *new_next = c_list->next->next;
        free(c_list->next);
        c_list->next = new_next;
        //Do not advance node; go back on the loop
        continue;
      }
      else{
        {
          char buff[128];
          memset(buff, 0, sizeof(buff));
          snprintf(buff, sizeof(buff) - 1, "Process %d still alive", c_list->next->pid);
          logmsg(WS_LOG_AUTISTIC, buff);
        }
      }
    }
    c_list = c_list->next;
  }while(c_list);
}

static void child_add(t_children *c_list, pid_t pid){
  assert(c_list);
  //Find list tail
  while(c_list->next != NULL){
    c_list = c_list->next;
  }
  //Add node
  c_list->next = (t_children*) malloc(sizeof(t_children));
  assert(c_list->next);
  c_list->next->next = NULL;
  c_list->next->pid  = pid;
}

// ------------------------------------------------------
// --- Websocket connection listener (parent process) ---
// ------------------------------------------------------

void ws_server(const ws_config *config){
  //Copy the config struct
  config_srv = *config;
  //Initialize children pid list
  //  First node is us parent process, so it will always be valid (and not checked)
  children.pid  = getpid();
  children.next = NULL;
  //Ignore the SIGCHLD signal to prevent ZOMBIES!
  signal(SIGCHLD, SIG_IGN);

  //Bind to a TCP socket and wait for an incoming connection.
  char msg_buffer[128];
  memset(msg_buffer, '\0', sizeof(msg_buffer));

  //Create socket descriptor
  int server_fd;
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
    snprintf(msg_buffer, sizeof(msg_buffer)-1, "Failed to create socket");
    die(WS_EXIT_NO_FD, msg_buffer);
  }

  //Bind to port
  int opt = 1;
  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
  {
    snprintf(msg_buffer, sizeof(msg_buffer)-1, "Failed to set socket options");
    die(WS_EXIT_OPTION_FAIL, msg_buffer);
  }
  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = htonl( INADDR_LOOPBACK ); //Bind to 127.0.0.1
  address.sin_port = htons( (uint16_t)config_srv.port );
  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
  {
    snprintf(msg_buffer, sizeof(msg_buffer)-1, "Failed to bind to %d.%d.%d.%d:%d",
        (ntohl(address.sin_addr.s_addr) >> 24) & 0xFF,
        (ntohl(address.sin_addr.s_addr) >> 16) & 0xFF,
        (ntohl(address.sin_addr.s_addr) >> 8 ) & 0xFF,
        (ntohl(address.sin_addr.s_addr) >> 0 ) & 0xFF,
        ntohs(address.sin_port)
    );
    die(WS_EXIT_BIND_FAIL, msg_buffer);
  }
  if (listen(server_fd, 1) < 0)
  {
    snprintf(msg_buffer, sizeof(msg_buffer)-1, "Failed to execute listen()");
    die(WS_EXIT_LISTEN_FAIL, msg_buffer);
  }
  else{
    snprintf(msg_buffer, sizeof(msg_buffer)-1, "Listening on %d.%d.%d.%d:%d",
        (ntohl(address.sin_addr.s_addr) >> 24) & 0xFF,
        (ntohl(address.sin_addr.s_addr) >> 16) & 0xFF,
        (ntohl(address.sin_addr.s_addr) >> 8 ) & 0xFF,
        (ntohl(address.sin_addr.s_addr) >> 0 ) & 0xFF,
        ntohs(address.sin_port)
    );
    logmsg(WS_LOG_INFO, msg_buffer);
    memset(msg_buffer, '\0', sizeof(msg_buffer));
  }

  //Wait for an incoming connection
  //ToDo Keep listening for more connections
  int listen_again = 1;
  while(listen_again){
    struct sockaddr_in peer_address;
    socklen_t peer_addrlen = sizeof(peer_address);
    int open_socket;
    if ((open_socket = accept(server_fd, (struct sockaddr *)&peer_address, (socklen_t*)&peer_addrlen)) < 0)
    {
      snprintf(msg_buffer, sizeof(msg_buffer)-1, "Failed to accept connection");
      die(WS_EXIT_ACCEPT_FAIL, msg_buffer);
    }

    //Connection accepted. Fork off to another process.
    sprintf(msg_buffer, "Connection from: %d.%d.%d.%d:%d",
      (ntohl(peer_address.sin_addr.s_addr) >> 24) & 0xFF,
      (ntohl(peer_address.sin_addr.s_addr) >> 16) & 0xFF,
      (ntohl(peer_address.sin_addr.s_addr) >> 8) & 0xFF,
      (ntohl(peer_address.sin_addr.s_addr) >> 0) & 0xFF,
      ntohs(peer_address.sin_port)
    );
    logmsg(WS_LOG_INFO, msg_buffer);
    memset(msg_buffer, '\0', sizeof(msg_buffer));

    pid_t fork_rv = fork();
    switch(fork_rv){
      case 0:
      //Child process. Go on handle the connection
      handle_connection(open_socket, &peer_address, peer_addrlen);
      listen_again = 0; //We don't wanna accept connections as a child.
      break;

      case -1:
      //Error
      snprintf(msg_buffer, sizeof(msg_buffer)-1, "Failed to fork worker process");
      die(WS_EXIT_FORK_FAIL, msg_buffer);
      break;

      default:
      //Parent process. Log the child PID and go on wait for someone else
      child_cleanup(&children);
      child_add(&children, fork_rv);
      break;
    }
  } //End of listen loop.
}

void ws_server_cleanup(){
  //Send SIGTERM/SIGKILL to all the children on children list
  //(But only if I am the parent process, of course)
  if(children.pid == getpid()){
    logmsg(WS_LOG_DEBUG, "Cleaning up server processes");
    child_cleanup(&children);
    //Send SIGTERM to all childs
    t_children *c = &children;
    while(c->next){
      {
        //Log message
        char msg_buffer[128];
        memset(msg_buffer, '\0', sizeof(msg_buffer));
        snprintf(msg_buffer, sizeof(msg_buffer)-1, "Sending SIGTERM to child process %d",
              c->next->pid
          );
        logmsg(WS_LOG_DEBUG, msg_buffer);
      }
      kill(c->next->pid, SIGTERM);
      c = c->next;
    }
    sleep(1);
    child_cleanup(&children);
    c = &children;
    while(c->next){
      {
        //Log message
        char msg_buffer[128];
        memset(msg_buffer, '\0', sizeof(msg_buffer));
        snprintf(msg_buffer, sizeof(msg_buffer)-1, "Sending SIGKILL to child process %d",
              c->next->pid
          );
        logmsg(WS_LOG_DEBUG, msg_buffer);
      }
      kill(c->next->pid, SIGKILL);
      c = c->next;
    }
  }
  else{
    logmsg(WS_LOG_DEBUG, "Child process; Skip child cleanup");
  }
}

// ------------------------------------------------------
// --- Websocket connection handling (worker process) ---
// ------------------------------------------------------

static const char html_header[] = 
    //Must have a %d (HTTP code)
    "<!DOCTYPE html>\n"
    "<html lang=\"en\">\n"
    "  <head>\n"
    "    <title>HTTP %d</title>\n"
    "  </head>\n"
    "  <body>\n";
static const char html_body[] =
    // Must have a %d (HTTP code) and %s (message); In that order
    "    <h1>HTTP %d</h1><pre>%s</pre>\n";
static const char html_footer[] = 
    "  </body>\n"
    "</html>";
static int socket_fd;
static const struct sockaddr_in * peer;

static void error_response(int code, const char* msg, const char* extra){
  char buff[128];
  int line_len;
  int response_size = 1024;

  {
    char log_msg[128];
    memset(log_msg, '\0', 128);
    if(extra){
      snprintf(log_msg, sizeof(log_msg), "Sending HTTP %d %s (%s)", code, msg, extra);
    }
    else{
      snprintf(log_msg, sizeof(log_msg), "Sending HTTP %d %s", code, msg);
    }
    logmsg(WS_LOG_CRITICAL, log_msg);
  }

  line_len = snprintf(buff, sizeof(buff), "HTTP/1.1 %d %s\r\n", code, msg);
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "X-Worker: %d\r\n", getpid());
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "X-Parent: %d\r\n", getppid());
  send(socket_fd, buff, line_len, 0);

  {
    char nothing = 0; //An empty `string`
    if(extra == NULL){ extra = &nothing; }
    line_len = snprintf(buff, sizeof(buff), "Content-Length: %d\r\n", response_size);
    send(socket_fd, buff, line_len, 0);
    line_len = snprintf(buff, sizeof(buff), "Content-Type: text/html; charset=utf8\r\n");
    send(socket_fd, buff, line_len, 0);
    line_len = snprintf(buff, sizeof(buff), "\r\n");
    send(socket_fd, buff, line_len, 0);

    //Send HTML
    { //Header
      line_len = snprintf(buff, sizeof(buff), html_header, code);
      response_size -= line_len;
      send(socket_fd, buff, line_len, 0);
    }
    { //Body
      line_len = snprintf(buff, sizeof(buff), html_body, code, extra);
      response_size -= line_len;
      send(socket_fd, buff, line_len, 0);
    }
    { //Footer
      line_len = snprintf(buff, sizeof(buff), "%s\n", html_footer);
      response_size -= line_len;
      send(socket_fd, buff, line_len, 0);
    }
    assert(response_size > 0);

    //Add padding (Spaces)
    for(int i = 0; i < response_size; i++){
      send(socket_fd, " ", 1, 0);
    }
  }

  close(socket_fd);
}

static void accept_response(const char* accept_key){
  char buff[128];
  int line_len;

  logmsg(WS_LOG_INFO, "Sending HTTP 101 response");

  //Send the accept request via HTTP 101 + key
  line_len = snprintf(buff, sizeof(buff), "HTTP/1.1 101 Switching Protocols\r\n");
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "Upgrade: websocket\r\n");
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "Connection: Upgrade\r\n");
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "X-Worker: %d\r\n", getpid());
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "X-Parent: %d\r\n", getppid());
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "Sec-WebSocket-Accept: %s\r\n", accept_key);
  send(socket_fd, buff, line_len, 0);
  line_len = snprintf(buff, sizeof(buff), "\r\n");
  send(socket_fd, buff, line_len, 0);
}

#define HTTP_413() do{ error_response(413, "URI Too Long", NULL);    }while(0)
#define HTTP_501() do{ error_response(501, "Not Implemented", NULL); }while(0)
#define HTTP_400() do{ error_response(400, "Bad Request", NULL);     }while(0)

#define HTTP_501_MSG(X) do{ error_response(501, "Not Implemented", X); }while(0)
#define HTTP_400_MSG(X) do{ error_response(400, "Bad Request", X);     }while(0)

#define HTTP_101_OK(K)  do{ accept_response(K); }while(0)

//Save the answer to websock key here
static char key_response[64];

//Parse a single header. String is guaranteed to be null-terminated.
static void parse_header(const char* header){
  size_t header_len = strlen(header);
  //Find the ':' character
  const char* header_split = strchr(header, ':');
  //If not found, warn and return
  if(header_split == NULL){
    ///@note maybe add the header in question
    logmsg(WS_LOG_WARNING, "Invalid header");
    return;
  }
  //If found, there must be at least one space after it
  header_split++;
  if(*header_split != ' '){
    logmsg(WS_LOG_WARNING, "Invalid header");
    return;
  }
  header_split++; //<-- Make header_split lay at the feet of header contents.

  //Iterate over known headers
  //Header 'Sec-WebSocket-Key' is of particular interest
  if     (strstr(header, "Sec-WebSocket-Key") == header){
    char msg[128];
    memset(msg, '\0', sizeof(msg));
    memset(key_response, '\0', sizeof(key_response));
    ws_key_challenge(header_split, key_response, sizeof(key_response)-1);
    snprintf(msg, sizeof(msg)-1, "WS Key: %s --> %s", header_split, key_response);
    logmsg(WS_LOG_DEBUG, msg);
  }
  else if(strstr(header, "Sec-WebSocket-Version") == header){
    ///Must be "13".
    if(strcmp("13", header_split) != 0){
      HTTP_400_MSG("Wrong WebSocket version");
      die(WS_EXIT_HTTP_ERROR, NULL);
    }
  }
  else if(strstr(header, "Upgrade") == header){
    ///Must be "websocket".
    if(strcmp("websocket", header_split) != 0){
      HTTP_400_MSG("Wrong Upgrade request");
      die(WS_EXIT_HTTP_ERROR, NULL);
    }
  }
  else if(strstr(header, "Connection") == header){
    ///Must be "Upgrade".
    if(strcmp("Upgrade", header_split) != 0){
      HTTP_400_MSG("Expected a connection upgrade");
      die(WS_EXIT_HTTP_ERROR, NULL);
    }
    logmsg(WS_LOG_DEBUG, header_split);
  }
}

static void parse_headers(){
  //Parse client request
  logmsg(WS_LOG_DEBUG, "Parsing request");
  {
    char request_line[1024];
    char c = '\0';
    int offset = 0;
    ssize_t read_size;
    while( (c != '\n') && (offset < sizeof(request_line)) ){
      read_size = recv(socket_fd, &c, 1, 0);
      if(read_size == 0){
        die(WS_EXIT_CONNECTION_CLOSED, "Connection closed while parsing request");
      }
      request_line[offset] = c;
      offset++;
    }
    //Offset _MUST_ be at least 2 (end-of-headers is the smallest "header")
    if(offset < 2){
      //Bad request
      HTTP_400();
      die(WS_EXIT_HTTP_ERROR, NULL);
    }
    //Check if loop exit reason is a buffer too large
    if(read_size == sizeof(request_line)){
      HTTP_413();
      die(WS_EXIT_HTTP_ERROR, NULL);
    }
    //If not, replace '\r' with '\0'
    request_line[offset - 2] = '\0';
    logmsg(WS_LOG_DEBUG, request_line);
  }

  //Parse headers
  int headers_ended = 0;
  while(!headers_ended){
    char request_line[1024];
    char c = '\0';
    int offset = 0;
    ssize_t read_size;
    while( (c != '\n') && (offset < sizeof(request_line)) ){
      read_size = recv(socket_fd, &c, 1, 0);
      if(read_size == 0){
        die(WS_EXIT_CONNECTION_CLOSED, "Connection closed while parsing headers");
      }
      request_line[offset] = c;
      offset++;
    }
    //Check if loop exit reason is a buffer too large
    if(read_size == sizeof(request_line)){
      //Skip header by reading until the first '\n' or the socket dies
      do{
        read_size = recv(socket_fd, &c, 1, 0);
        if(read_size == 0){
          die(WS_EXIT_CONNECTION_CLOSED, "Connection closed while skipping a header");
        }
      }while(c != '\n');
    }
    //Offset _MUST_ be at least 2 (end-of-headers is the smallest "header")
    if(offset < 2){
      //Bad request
      HTTP_400();
      die(WS_EXIT_HTTP_ERROR, NULL);
    }
    //Check if the exit reason is end-of-headers ("\r\n")
    if(memcmp(request_line, "\r\n", 2) == 0){
      //No more headers
      headers_ended = 1;
      logmsg(WS_LOG_DEBUG, "No more headers");
      //just break. headers_ended seems redundant.
      break;
    }
    //Otherwise, parse header
    request_line[offset - 2] = '\0';
    logmsg(WS_LOG_DEBUG, request_line);
    parse_header(request_line);
  }

  //If at least the key_response has been read, answer wit a 101
  if(strlen(key_response)> 0){
    HTTP_101_OK(key_response);
    logmsg(WS_LOG_INFO, "WS handshake suceeded");
  }
  else{
    HTTP_400_MSG("Missing Sec-WebSocket-Key");
    logmsg(WS_LOG_CRITICAL, "No Sec-WebSocket-Key found");
    die(WS_EXIT_HTTP_ERROR, NULL);
  }
}

//--- Child process entry point ---
int handle_connection(int sock_fd, const struct sockaddr_in * peer_address, socklen_t peer_addrlen){
  char msg_buffer[128];
  memset(msg_buffer, '\0', sizeof(msg_buffer));

  //Show peer info
  {
    snprintf(msg_buffer, sizeof(msg_buffer)-1, "Handling connection from: %d.%d.%d.%d:%d",
      (ntohl(peer_address->sin_addr.s_addr) >> 24) & 0xFF,
      (ntohl(peer_address->sin_addr.s_addr) >> 16) & 0xFF,
      (ntohl(peer_address->sin_addr.s_addr) >> 8) & 0xFF,
      (ntohl(peer_address->sin_addr.s_addr) >> 0) & 0xFF,
      ntohs(peer_address->sin_port)
    );
    logmsg(WS_LOG_INFO, msg_buffer);
    memset(msg_buffer, '\0', sizeof(msg_buffer));
  }

  //Copy data to static variables
  socket_fd = sock_fd;
  peer = peer_address;

  //Parse client request
  parse_headers();

  //If we have survived this long, it means the WS handshake has suceeded.

  //Resolve the target url
  logmsg(WS_LOG_DEBUG, "Resolving host:");
  logmsg(WS_LOG_DEBUG, config_srv.connect_url);
  logmsg(WS_LOG_WARNING, "Target resolving not implemented");

  //Start listening to and sending
  //WS frames over the wire.

  //Prepare for poll()-ing on the socket
  struct pollfd poll_array[1];

  //Add websocket fd
  poll_array[0].fd = sock_fd;
  poll_array[0].events = POLLIN;
  poll_array[0].revents = 0;

  int worker_alive = 1;
  while(worker_alive){
    poll(poll_array, sizeof(poll_array), -1);
    //check reason poll() returned
    if(poll_array[0].revents & POLLIN){
      //Data ready to be read
      ///@todo Do something, as of now, just empty the buffer.
      uint8_t buff[1024];
      ssize_t read_size = recv(sock_fd, buff, sizeof(buff), 0);
      if(read_size > 0){
        logmsg(WS_LOG_DEBUG, "Data ready on socket");
        if(config_srv.verbosity >= WS_LOG_AUTISTIC){
          debug_dump(buff, read_size);
        }
        //send(sock_fd, buff, read_size, 0);
      }
      else{
        //No more data. Exit
        logmsg(WS_LOG_DEBUG, "No more data on socket");
        worker_alive = 0;
      }
    }
    else if(poll_array[0].revents & POLLERR){
      //Error on socket descriptor
      logmsg(WS_LOG_CRITICAL, "Socket error after poll()");
      worker_alive = 0;
    }
    else if(poll_array[0].revents & POLLHUP){
      //Peer closed connection
      logmsg(WS_LOG_INFO, "Connection reset by peer");
      worker_alive = 0;
    }
    else if(poll_array[0].revents & POLLNVAL){
      //FD not valid
      logmsg(WS_LOG_CRITICAL, "Invalid socket descriptor after poll()");
      worker_alive = 0;
    }
    //Clear revents
    poll_array[0].revents = 0;
  }

  //Close connection
  close(socket_fd);

  //End process
  die(WS_EXIT_OK, "Worker finished.");
  return 0;
}
