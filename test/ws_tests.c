//Test the correctness of the unmasking algorithm
//as per RFC description
#include <stdio.h>
#include <string.h>
#include <wsports.h>
#include <websocket.h>

//Full-frames
static const uint8_t frame_masked[]   = {0x81, 0x85, 0x37, 0xfa, 0x21, 0x3d, 0x7f, 0x9f, 0x4d, 0x51, 0x58};
static const uint8_t frame_unmasked[] = {0x81, 0x05,                         0x48, 0x65, 0x6c, 0x6c, 0x6f};

//Headers
static const uint8_t frame_header_64k[]  = {0x81, 0x7E, 0xAA, 0xBB};
static const uint64_t payload_size_64k   = 0xAABBU;

static const uint8_t frame_header_huge[] = {0x81, 0x7F, 0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88};
static const uint64_t payload_size_huge  = 0xFFEEDDCCBBAA9988U;

static const uint8_t frame_header_64k_masked[]  = {0x81, 0xFE, 0xAA, 0xBB, 0x00, 0x01, 0x02, 0x03};
static const uint64_t payload_size_64k_masked   = 0xAABBU;

static const uint8_t frame_header_huge_masked[] = {0x81, 0xFF, 0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x00, 0x01, 0x02, 0x03};
static const uint64_t payload_size_huge_masked  = 0xFFEEDDCCBBAA9988U;

static int test_header_size(){
  int all_ok = 1;

  all_ok = all_ok && (ws_header_size(frame_header_64k) == sizeof(frame_header_64k));
  all_ok = all_ok && (ws_header_size(frame_header_huge) == sizeof(frame_header_huge));
  all_ok = all_ok && (ws_header_size(frame_header_64k_masked) == sizeof(frame_header_64k_masked));
  all_ok = all_ok && (ws_header_size(frame_header_huge_masked) == sizeof(frame_header_huge_masked));

  if(all_ok){
    return 1;
  }
  else{
    puts(__FILE__ " - test_header_size failed." );
    return 0;
  }
}

static int test_payload_size(){
  int all_ok = 1;

  all_ok = all_ok && (ws_payload_size(frame_header_64k) == payload_size_64k);
  all_ok = all_ok && (ws_payload_size(frame_header_huge) == payload_size_huge);
  all_ok = all_ok && (ws_payload_size(frame_header_64k_masked) == payload_size_64k_masked);
  all_ok = all_ok && (ws_payload_size(frame_header_huge_masked) == payload_size_huge_masked);

  if(all_ok){
    return 1;
  }
  else{
    puts(__FILE__ " - test_payload_size failed." );
    return 0;
  }
}

static int test_structs(){
  const struct ws_header_small *frame  = (const struct ws_header_small*) frame_masked;
  int all_ok = 1;

  //Test small struct
  {
    all_ok = all_ok && (frame->flags == frame_masked[0]);
    all_ok = all_ok && (frame->mask_n_length == frame_masked[1]);
    all_ok = all_ok && (* (uint32_t*)frame->masking_key == *(uint32_t*)(frame_masked + 2));
  }

  if(all_ok){
    return 1;
  }
  else{
    puts(__FILE__ " - test_structs failed." );
    return 0;
  }
}

static int test_validate(){
  int all_ok = 1;

  //Small frames, correct size.
  all_ok = all_ok && (ws_validate_frame(frame_masked,   sizeof(frame_masked)));
  all_ok = all_ok && (ws_validate_frame(frame_unmasked, sizeof(frame_unmasked)));
  all_ok = all_ok && (ws_validate_frame(frame_masked,   sizeof(frame_masked))   == sizeof(frame_masked));
  all_ok = all_ok && (ws_validate_frame(frame_unmasked, sizeof(frame_unmasked)) == sizeof(frame_unmasked));

  //Small frames, size too small (by one)
  all_ok = all_ok && (!ws_validate_frame(frame_masked,   sizeof(frame_masked)   - 1));
  all_ok = all_ok && (!ws_validate_frame(frame_unmasked, sizeof(frame_unmasked) - 1));

  if(all_ok){
    return 1;
  }
  else{
    puts(__FILE__ " - test_validate failed." );
    return 0;
  }
}

static int test_unmask(){
  uint8_t test_vector[]       = { 0x37, 0xfa, 0x21, 0x3d, 0x7f, 0x9f, 0x4d, 0x51, 0x58 };
  const uint8_t test_result[] =                         { 0x48, 0x65, 0x6c, 0x6c, 0x6f };

  ws_unmask_payload(test_vector, sizeof(test_vector));
  if(memcmp(test_vector + WS_MASK_KEY_SIZE, test_result, sizeof(test_result)) == 0){
    return 1;
  }
  else{
    puts(__FILE__ " - test_unmask failed." );
    printf("Expected == Found (?)\n");
    for(int i = 0; i < sizeof(test_result); i++){
      const uint8_t* unmasked = test_vector + WS_MASK_KEY_SIZE;
      printf("%02X == %02X (%s)\n", test_result[i], unmasked[i], (test_result[i] == unmasked[i]) ? "OK" : "FAIL");
    }
    return 0;
  }
}

int main(int argc, char** argv){
  (void) argc;
  (void) argv;

  int all_ok = 1;
  puts("--- Test start ---");
  all_ok = all_ok && test_unmask();
  all_ok = all_ok && test_validate();
  all_ok = all_ok && test_structs();
  all_ok = all_ok && test_payload_size();
  all_ok = all_ok && test_header_size();
  puts("--- Test end ---");

  //If all OK return 0 (shell-style OK ;)
  return all_ok ? 0 : 1;
}
