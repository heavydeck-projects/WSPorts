//Test the correctness of the Sec-WebSocket-Key response
//as per RFC description
#include <stdio.h>
#include <string.h>
#include <sha1.h>
#include <cbase64.h>
#include <wsports.h>

#define CLIENT_KEY "dGhlIHNhbXBsZSBub25jZQ=="
//WS_GUID defined in wsports.h
#define EXPECTED_RESPONSE "s3pPLMBiTxaQ9kYGzzhZRbK+xOo="
/*static const uint8_t expected_hash[20] = {
    0xb3, 0x7a, 0x4f, 0x2c, 0xc0, 0x62, 0x4f, 0x16, 0x90, 0xf6,
    0x46, 0x06, 0xcf, 0x38, 0x59, 0x45, 0xb2, 0xbe, 0xc4, 0xea,
};*/

int main(int argc, char** argv){
  char response_buffer[128];
  memset(response_buffer, '\0', sizeof(response_buffer));

  ws_key_challenge(CLIENT_KEY, response_buffer, sizeof(response_buffer));
  if(strcmp(EXPECTED_RESPONSE, response_buffer) == 0){
    //Test success
    printf("Test OK.\n");
    return 0;
  }
  else {
    //Test error
    printf("Test failed!\n");
    printf("  Expected: '%s'\n", EXPECTED_RESPONSE);
    printf("       Got: '%s'\n", response_buffer);
    return 1;
  }
}
