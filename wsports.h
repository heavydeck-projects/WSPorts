#ifndef __WSPORTS_H
#define __WSPORTS_H

//Put all the platform-dependant includes here
#ifdef __linux__
  //Linux headers
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <unistd.h>
  #include <poll.h>
  #include "websocket.h"
#elif __CYGWIN__
  //Cygwin uses the same as linux...
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <netinet/in.h>
  #include <unistd.h>
  #include <poll.h>
  #include "websocket.h"
  //... but has some undefinitions from windows
  #ifndef SO_REUSEPORT
    #define SO_REUSEPORT 0
  #endif
#elif _WIN32
  #include <winsock2.h>
  #include "websocket.h"
  typedef int socklen_t; //<-- Undefined on windows
  #ifndef SO_REUSEPORT
    #define SO_REUSEPORT 0
  #endif
  //Many functions will not be available on non-cygwin windows.
  //Added here to make it compile. It will fail to link though...
  int getpid();
  int getppid();
  //Same happens for signals
  #define SIGUSR1 0
  #define SIGCHLD 0
  #define SIGKILL 0
  //And a ton of poll.h macros & types
  #define POLLERR 0
  #define POLLHUP 0
  #define POLLNVAL 0
  #define POLLIN 0
  struct pollfd { int events; int revents; int fd; };
  int poll(void*, int, int);
#else
  #error Unknown platform
#endif

// --- Constants ---
//Version (defaults if none provided)
#ifndef WS_VERSION_MAJOR
#define WS_VERSION_MAJOR 0
#endif
#ifndef WS_VERSION_MINOR
#define WS_VERSION_MINOR 0
#endif
#ifndef WS_VERSION_BUILD
#define WS_VERSION_BUILD 0
#endif

// --- Types ---
enum ws_exitcodes{
  WS_EXIT_OK = 0,
  WS_EXIT_INTERRUPTED,
  WS_EXIT_BAD_ARGUMENTS,

  WS_EXIT_NO_FD,
  WS_EXIT_OPTION_FAIL,
  WS_EXIT_BIND_FAIL,
  WS_EXIT_LISTEN_FAIL,
  WS_EXIT_ACCEPT_FAIL,
  WS_EXIT_FORK_FAIL,

  WS_EXIT_CONNECTION_CLOSED,

  WS_EXIT_HTTP_ERROR,

  WS_EXIT_GENERIC_ERROR,
  WS_EXIT_LAST
};

static const char* exit_names[] = {
  "WS_EXIT_OK",
  "WS_EXIT_INTERRUPTED",
  "WS_EXIT_BAD_ARGUMENTS",

  "WS_EXIT_NO_FD",
  "WS_EXIT_OPTION_FAIL",
  "WS_EXIT_BIND_FAIL",
  "WS_EXIT_LISTEN_FAIL",
  "WS_EXIT_ACCEPT_FAIL",
  "WS_EXIT_FORK_FAIL",

  "WS_EXIT_CONNECTION_CLOSED",

  "WS_EXIT_HTTP_ERROR",

  "WS_EXIT_GENERIC_ERROR",
  "WS_EXIT_LAST"
};


enum ws_loglevel{
  WS_LOG_CRITICAL = 0,
  WS_LOG_SEVERE,
  WS_LOG_WARNING,
  WS_LOG_INFO,
  WS_LOG_DEBUG,
  WS_LOG_AUTISTIC
};

/**Configuration struct, contains the parsed cli arguments.
  Caller must free up the resources, callee must deep-copy the
  struct if it wants to keep it around past the fork() call.
*/
struct ws_config_s{
  int is_server;     /**<-- WS Server mode. */
  int is_client;     /**<-- WS Client mode. */
  int port;          /**<-- Listen port, be it WebSocket endpoint (server) or TCP endpoint (client). */
  int is_raw;        /**<-- Skip RFC data framing. */
  int verbosity;     /**<-- Maximum verbosity level. */
  char *request_url; /**<-- (Server-only) The expected URL within the WS request. */
  char *connect_url; /**<-- The websocket endpoint (client) or the TCP target (server). */
};
typedef struct ws_config_s ws_config;

#ifndef NDEBUG
  //Default options (debug)
  #define WS_CONFIG_DEFAULT {0, 0, 2233, 0, WS_LOG_AUTISTIC, (char*)NULL, (char*)NULL}
#else
  //Default options
  #define WS_CONFIG_DEFAULT {0, 0, 2233, 0, WS_LOG_INFO, (char*)NULL, (char*)NULL}
#endif

// --- wsports utility functions ---
int main_inner(int argc, char** argv);
void logmsg(int level, const char* msg);
void debug_dump(const void* buff, ssize_t size);
void die(int return_value, const char* msg);

// --- Server ---
void ws_server(const ws_config *config);
void ws_server_cleanup();
int handle_connection(int socket_fd, const struct sockaddr_in * peer_address, socklen_t peer_addrlen);

// --- Client ---
void ws_client(const ws_config *config);

#endif
