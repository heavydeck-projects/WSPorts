# WSPorts -- WebSocket Ports

Blah blah.

## Nginx configuration

```
  location /ws/ {
    proxy_pass http://127.0.0.1:2233;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
  }
```

## Third-party code

* Public domain Base64 implementation by Jordan Cristiano [(repository)][base64]

## Reference documentation

* [The websocket specification RFC6455][rfc6455]
* [Sha-1 reference implementation RFC3174][rfc3174]


[rfc6455]: https://tools.ietf.org/html/rfc6455
[rfc3174]: https://tools.ietf.org/html/rfc3174
[base64]: https://github.com/SizzlingCalamari/cbase64
