#ifndef __BASE64_H
#define __BASE64_H

char* B64EncodeData(const unsigned char* data_in, unsigned int length_in, unsigned int* length_out);

#endif

