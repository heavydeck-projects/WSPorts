#define CBASE64_IMPLEMENTATION
#include "cbase64.h"
#include "base64.h"

#include <stdlib.h>

char* B64EncodeData(const unsigned char* data_in, unsigned int length_in, unsigned int* length_out)
{
    const unsigned int encodedLength = cbase64_calc_encoded_length(length_in);
    char* codeOut = (char*)malloc(encodedLength);
    char* codeOutEnd = codeOut;

    cbase64_encodestate encodeState;
    cbase64_init_encodestate(&encodeState);
    codeOutEnd += cbase64_encode_block(data_in, length_in, codeOutEnd, &encodeState);
    codeOutEnd += cbase64_encode_blockend(codeOutEnd, &encodeState);

    *length_out = (codeOutEnd - codeOut);
    return codeOut;
}
