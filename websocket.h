//Websocket encoding/decoding
#ifndef __WEBSOCKET_H
#define __WEBSOCKET_H

#include <stdint.h>
#include <stddef.h>

//RFC6455 constants
#define WS_GUID "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

//Program constants
#define WS_HEADER_MAX_LEN 1024

//Types

#pragma pack(1)
///Websocket header for packets up to 64K.
struct ws_header{
  uint8_t  flags;           ///< Frame flags & opcode number.
  uint8_t  mask_n_length;   ///< Frame length & mask flag.
  uint16_t extended_length; ///< Frame length for len > 125 and up to 64K.
  uint8_t  masking_key[4];  ///< Websocket masking key (4bytes).
};
///Websocket header for small (<126byte) packets.
struct ws_header_small{
  uint8_t  flags;           ///< Frame flags & opcode number.
  uint8_t  mask_n_length;   ///< Frame length & mask flag.
  uint8_t  masking_key[4];  ///< Websocket masking key (4bytes).
};
///Websocket header for large (>64K) packets
struct ws_header_huge{
  uint8_t  flags;             ///< Frame flags & opcode number.
  uint8_t  mask_n_length;     ///< Frame length & mask flag.
  uint64_t extended_length;   ///< Frame length for len > 64K.
  uint8_t  masking_key[4];    ///< Websocket masking key (4bytes).
};
#pragma pack()

//Masks for flags & arguments
#define WS_FLAG_FIN    (1 << 7)
#define WS_FLAG_RSV1   (1 << 6)
#define WS_FLAG_RSV2   (1 << 5)
#define WS_FLAG_RSV3   (1 << 4)
#define WS_MASK_OPCODE (0x0F)

#define WS_FLAG_MASK   (1 << 7)
#define WS_MASK_LENGTH (0x7F)

//More constants
#define WS_LENGTH_16  (126)
#define WS_LENGTH_64  (127)
#define WS_MASK_KEY_SIZE 4

///Generate the response hash for the client request.
size_t ws_key_challenge(const char *client_key, char* server_response, size_t len);

/**Unmask ws payload.
 * @param buff Pointer to buffer, positioned at the begining of the mask key.
 * @param len  Buffer length, usually key + payload.
 */
void ws_unmask_payload(void* buff, size_t length);

/**Unmask ws frame if masked, otherwise frame is left as-is.
 * If the function unmasks the frame, it WILL NOT correct the 4-byte payload offset
 * from the masking key, use ws_frame_payload to get payload pointer.
 * @param buff Pointer to buffer, positioned at the begining of the mask key.
 * @param len  Frame buffer length.
 */
void ws_unmask_frame(void* buff, size_t len);

///Return true if frame is masked.
int ws_is_masked(const void* buff);

///Return ws header size.
size_t ws_header_size(const void* buff);

///Return a pointer to the ws frame payload.
void* ws_frame_payload (void *buff);

///Payload size as reported by header.
uint64_t ws_payload_size(const void *buff);

/**Validates whether a buffer of size len contains a valid websocket frame.
 * @param buff buffer containing the frame.
 * @param len  size of the buffer
 * @returns >0 the size of the frame if a valid frame is found.
 */
uint64_t ws_validate_frame(const void* buff, size_t len);

#endif

