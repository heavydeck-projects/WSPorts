#include "wsports.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <limits.h>

#define OPTPARSE_IMPLEMENTATION
#define OPTPARSE_API static
#include "optparse.h"

#ifdef WITH_GNUTLS
  #include <gnutls/gnutls.h>
#endif

//Forward declaration of sig handler
void sig_handler(int signo);

static int loglevel = INT_MAX;
void logmsg(int level, const char* msg){
  if(msg && (level <= loglevel)){
    fprintf(stderr, "[%d] %s\n", getpid(), msg);
  }
  fflush(stderr);
}

void debug_dump(const void* buff, ssize_t size){
  char *c = (char*)buff;
  //Print while lines

  logmsg(WS_LOG_AUTISTIC, "|---------------------------------------------| |--------------|");
  while(size > 0){
    //16*3 dump chars + space/tab + 16 printables + newline + null
    char dump_str[(16*3) + 1 + 16 + 1 + 1];
    char *dump_c = dump_str;
    //Print hex dump
    for(int i = 0; i < 16; i++){
      if(size > 0){
        static const char hex_lut[] = "0123456789ABCDEF";
        *dump_c = hex_lut[((*c) >> 4) & 0x0F]; dump_c++;
        *dump_c = hex_lut[((*c) >> 0) & 0x0F]; dump_c++;
        *dump_c = 0x20; dump_c++;
      }
      else{
        *dump_c = 0x20; dump_c++;
        *dump_c = 0x20; dump_c++;
        *dump_c = 0x20; dump_c++; //Spaces
      }
      c++;
      size--;
    }
    //Restore size
    size += 16;
    c -= 16;
    //Print printable chars
    for(int i = 0; i < 16; i++){
      if(size > 0){
        if((*c >= 0x20) && (*c <= 0x7E)){
          *dump_c = *c;
        }
        else{
          *dump_c = 0x2E;
        }
      }
      else{
        *dump_c = 0x20;
      }
      dump_c++;
      buff++;
      size--;
    }
    *dump_c = 0x00;
    logmsg(WS_LOG_AUTISTIC, dump_str);
  }
  logmsg(WS_LOG_AUTISTIC, "|---------------------------------------------| |--------------|");
}

static int do_server_cleanup = 0;
//static int do_client_cleanup = 0;
void die(int exit_code, const char* msg){
  const char *no_msg = "";
  if(msg == NULL){
    msg = no_msg;
  }
  if(WS_LOG_CRITICAL <= loglevel){
    fprintf(stderr, "[%d] exit(%d) %s %s\n", getpid(), exit_code, exit_names[exit_code], msg);
    fflush(stderr);
  }
  //Cleanup server (if we are server)
  if(do_server_cleanup){
    ws_server_cleanup();
  }
  exit(exit_code);
}

// --- Main and argument parsing ---

static const char help_short [] =
  "Client usage: wsports -c [OPTIONS] [--] <HTTP_URL>\n"
  "Server usage: wsports -s [OPTIONS] [--] <ADDRESS:PORT>\n"
  ;
static const char help_long[] =
  "General options:\n"
  "  -c  --client    Launch WSPorts in client mode; Connects to a websocket\n"
  "  -s  --server    Launch WSPorts in server mode; Accepts websocket connections\n"
  "  -r  --raw       Do not encapsulate data in frames (non RFC compliant)\n"
  "  -p  --port      TCP port to listen on; Port 2233 by default\n"
  "  -P  --path      Which websocket URL path to accept; Any by default\n"
  "  -V  --version   Show version and builtin features\n"
  "  -v  --verbose   Increase verbosity level\n"
  "  -q  --quiet     Disable all output\n"
  "  -h  --help      Show this help thing\n"
#ifdef WITH_GNUTLS
  "TLS options:\n"
  "  (Nothing here yet)\n"
#else
  "This WSPorts executable has been built WITHOUT TLS support\n"
#endif
  ;

static void show_help(int long_help){
  printf(help_short);
  if(long_help){
    puts("");
    printf(help_long);
  }
}

//Print version and enabled features
static void show_version(){
  printf("WSPorts version %d.%d (Build %X)\n",
    WS_VERSION_MAJOR,
    WS_VERSION_MINOR,
    WS_VERSION_BUILD
  );

  //GnuTLS support?
  printf("  - GnuTLS support: ");
#ifdef WITH_GNUTLS
  printf("Enabled (Version %s)\n", gnutls_check_version(NULL));
#else
  printf("Disabled\n");
#endif
}

int main_inner(int argc, char** argv){
  //Parse CLI arguments
  struct optparse_long longopts[] = {
    {"client",  'c', OPTPARSE_NONE},
    {"server",  's', OPTPARSE_NONE},
    {"port",    'p', OPTPARSE_REQUIRED},
    {"path",    'P', OPTPARSE_REQUIRED},
    {"help",    'h', OPTPARSE_NONE},
    {"version", 'V', OPTPARSE_NONE},
    {"verbose", 'v', OPTPARSE_NONE},
    {"quiet",   'q', OPTPARSE_NONE},
    {"raw",     'r', OPTPARSE_NONE},
    {0}
  };
  struct optparse options;
  optparse_init(&options, argv);

  //Arguments
  struct ws_config_s config = WS_CONFIG_DEFAULT;
  loglevel = config.verbosity;

  //Iterate over options
  int option;
  while((option = optparse_long(&options, longopts, NULL)) != -1){
    switch(option){
    case 'h': /* --help */
    {
      show_help(1);
      return WS_EXIT_OK;
    }

    case 'c': /* --client */
    {
      logmsg(WS_LOG_AUTISTIC, "Option: --client");
      if(config.is_server){
        logmsg(WS_LOG_WARNING, "Mixed --server and --client options; Using only the last one.");
      }
      config.is_server = 0;
      config.is_client = 1;
      break;
    }

    case 's': /* --server */
    {
      logmsg(WS_LOG_AUTISTIC, "Option: --server");
      if(config.is_client){
        logmsg(WS_LOG_WARNING, "Mixed --server and --client options; Using only the last one.");
      }
      config.is_server = 1;
      config.is_client = 0;
      break;
    }

    case 'p': /* --port=<PORT> */
    {
      config.port = atoi(options.optarg);
      {
        char buff[64];
        memset(buff, '\0', sizeof(buff));
        snprintf(buff, sizeof(buff) - 1, "Option: --port %d", config.port);
        logmsg(WS_LOG_AUTISTIC, buff);
      }
      break;
    }

    case 'P': /* --path=<URL> */
    {
      size_t url_size = strlen(options.optarg);
      config.request_url = malloc(url_size + 1);
      memset(config.request_url, '\0', url_size + 1);
      strncpy(config.request_url, options.optarg, url_size);
      logmsg(WS_LOG_AUTISTIC, "Option: --path:");
      logmsg(WS_LOG_AUTISTIC, config.request_url);
      if(config.request_url){
        free(config.request_url);
        logmsg(WS_LOG_WARNING, "Multiple --path; Only last one will be used");
      }
      break;
    }

    case 'r': /* --raw */
    {
      config.is_raw = 1;
      logmsg(WS_LOG_AUTISTIC, "Option: --raw");
      break;
    }

    case 'v': /* --verbose */
    {
      config.verbosity++;
      loglevel = config.verbosity;
      {
        char buff[64];
        memset(buff, '\0', sizeof(buff));
        snprintf(buff, sizeof(buff) - 1, "Option: --verbose (Current verbosity: %d)", config.verbosity);
        logmsg(WS_LOG_AUTISTIC, buff);
      }
      break;
    }

    case 'q': /* --quiet */
    {
      config.verbosity = WS_LOG_CRITICAL - 1;
      loglevel = config.verbosity;
      logmsg(WS_LOG_AUTISTIC, "Option: --quiet");
      break;
    }

    case 'V': /* --version */
    {
      show_version();
      return WS_EXIT_OK;
    }

    case '?':
    {
      logmsg(WS_LOG_CRITICAL, "Unknown options");
      show_help(0);
      return WS_EXIT_BAD_ARGUMENTS;
    }

    }
  }

  logmsg(WS_LOG_AUTISTIC, "Main options ended, parsing URLs");
  //Remaining arguments are URLs
  char *arg;
  while ((arg = optparse_arg(&options))){
    if(config.connect_url){
      free(config.connect_url);
      logmsg(WS_LOG_WARNING, "Multiple target URLs; Only the last one will be used");
    }
    size_t connect_url_size = strlen(arg);
    config.connect_url = malloc(connect_url_size + 1);
    memset(config.connect_url, '\0', connect_url_size + 1);
    strncpy(config.connect_url, arg, connect_url_size);

    logmsg(WS_LOG_AUTISTIC, "Option: -- target URL:");
    logmsg(WS_LOG_AUTISTIC, config.connect_url);
  }

  //both client/server require an URL argument
  if(config.is_server || config.is_client){
    if(config.connect_url == NULL){
      die(WS_EXIT_BAD_ARGUMENTS, "A target URL is required");
    }
  }

  //End of option parsing
  logmsg(WS_LOG_AUTISTIC, "End of command-line parsing");

  //Some executable/architecture sanity checks
  {
    if(sizeof(size_t) < sizeof(uint64_t)){
      logmsg(WS_LOG_WARNING, "Type size_t is less than 64bit, this may cause issues");
    }
  }

  //Past this point, there might be dynamic memory used by the
  //various URLs, so we use a do/while(false) to allow the free()
  //of resources on error using a break statement.

  int return_value = WS_EXIT_OK;
  do{
    // --- Further validation of arguments ---

    //If neither client nor server, show small help
    if(config.is_client == config.is_server){
      show_help(0);
      return_value = WS_EXIT_BAD_ARGUMENTS;
      break;
    }

    //Check bad port numbers
    if((config.port < 1) || (config.port > 0xFFFF)){
      char msg[128];
      memset(msg, '\0', sizeof(msg));
      sprintf(msg, "Invalid port: %d", config.port);
      logmsg(WS_LOG_CRITICAL, msg);
      return_value = WS_EXIT_BAD_ARGUMENTS;
      break;
    }


    // --- Call server/client after argument validation ---

    //Bind signal handler
    signal(SIGINT, sig_handler);
    signal(SIGTERM, sig_handler);
    signal(SIGUSR1, sig_handler);

    //If server, await connections
    if(config.is_server){
      do_server_cleanup = 1; //<-- Cleanup server resources on die().
      ws_server(&config);
    }
    else{
      ws_client(&config);
    }

  }while(0);

  //Free up resources
  if(config.request_url){ free(config.request_url); }
  if(config.connect_url){ free(config.connect_url); }
  //Exit
  return return_value;
}

void sig_handler(int signo){
  if ((signo == SIGINT) || (signo == SIGTERM)){
    //Do cleanup

    //Exit
    die(WS_EXIT_INTERRUPTED, "Interrupted!");
  }
  else if(signo == SIGUSR1){
    ; //Do nothing, just log at most
    logmsg(WS_LOG_AUTISTIC, "Received SIGUSR1 (ignored)");
  }
}
