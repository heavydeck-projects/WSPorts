//Websocket encoding/decoding
#include "wsports.h"
#include "websocket.h"
#include "sha1.h"
#include "base64.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

size_t ws_key_challenge(const char *client_key, char* server_response, size_t len){
  //Calculate SHA-1 of client_key
  SHA1Context sha;
  uint8_t digest[20];
  SHA1Reset(&sha);
  SHA1Input(&sha, (uint8_t*)client_key, strlen(client_key));
  //Add GUID to hash
  SHA1Input(&sha, (uint8_t*)WS_GUID, strlen(WS_GUID));
  //Finalize hash
  SHA1Result(&sha, digest);

  //Log hash value
  {
    char msg_hash[128];
    memset(msg_hash, '\0', sizeof(msg_hash));
    snprintf(msg_hash, sizeof(msg_hash) - 1, "Response hash: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X",
      digest[0] & 0xFF, digest[1] & 0xFF, digest[2] & 0xFF, digest[3] & 0xFF, digest[4] & 0xFF,
      digest[5] & 0xFF, digest[6] & 0xFF, digest[7] & 0xFF, digest[8] & 0xFF, digest[9] & 0xFF,
      digest[10] & 0xFF, digest[11] & 0xFF, digest[12] & 0xFF, digest[13] & 0xFF, digest[14] & 0xFF,
      digest[15] & 0xFF, digest[16] & 0xFF, digest[17] & 0xFF, digest[18] & 0xFF, digest[19] & 0xFF
    );
    logmsg(WS_LOG_DEBUG, msg_hash);
  }
  //Base64 encode the bytes
  unsigned int encoded_len = 0;
  const char* encoded_digest = B64EncodeData(
    (const unsigned char*)digest,
    sizeof(digest),
    &encoded_len
  );
  len = (encoded_len > len) ? len : encoded_len;
  memcpy(server_response, encoded_digest, len);
  server_response[encoded_len] = '\0';
  free((void*) encoded_digest);
  return 0;
}

void ws_unmask_payload(void* buff, size_t len){
  assert(len >= 4); //< Key size (4)
  uint8_t *key     = (uint8_t*)buff;
  uint8_t *payload = ((uint8_t*)buff) + 4;
  uint_fast8_t key_index = 0;
  len -= 4;
  while(len){
    *payload = (*payload) ^ key[key_index];
    key_index = (key_index + 1) % 4;
    len--;
    payload++;
  }
}

int ws_is_masked(const void* buff){
  const struct ws_header_small *frame_small  = (const struct ws_header_small*) buff;
  if(frame_small->mask_n_length & WS_FLAG_MASK){
    return 1;
  }
  else{
    return 0;
  }
}

size_t ws_header_size(const void* buff){
  const struct ws_header_small *frame_small  = (const struct ws_header_small*) buff;
  const struct ws_header       *frame_medium = (const struct ws_header*) buff;
  const struct ws_header_huge  *frame_large  = (const struct ws_header_huge*) buff;
  size_t header_size;

  //Check header size
  if ( (frame_small->mask_n_length & WS_MASK_LENGTH) == WS_LENGTH_16 ){
    //64K MAX frame
    header_size = sizeof(struct ws_header);
  }
  else if ( (frame_small->mask_n_length & WS_MASK_LENGTH) == WS_LENGTH_64 ){
    //2^64 MAX frame
    header_size = sizeof(struct ws_header_huge);
  }
  else{
    //Otherwise: Small frame
    header_size = sizeof(struct ws_header_small);
  }

  //Check if it contains a mask key.
  if(ws_is_masked(buff)){
    //If masked, do nothing, struct already contains the mask key.
    ;
  }
  else{
    //If not, subtract the size
    header_size -= WS_MASK_KEY_SIZE;
  }

  return header_size;
}

uint64_t ws_payload_size(const void *buff){
  const struct ws_header_small *frame_small  = (const struct ws_header_small*) buff;
  const struct ws_header       *frame_medium = (const struct ws_header*) buff;
  const struct ws_header_huge  *frame_large  = (const struct ws_header_huge*) buff;
  uint64_t payload_size;

  //Check header size
  if ( (frame_small->mask_n_length & WS_MASK_LENGTH) == WS_LENGTH_16 ){
    //64K MAX frame
    payload_size = ntohs(frame_medium->extended_length);
  }
  else if ( (frame_small->mask_n_length & WS_MASK_LENGTH) == WS_LENGTH_64 ){
    //2^64 MAX frame
    const uint32_t *size_l = (uint32_t*)( &(frame_large->extended_length) ) + 1;
    const uint32_t *size_h = (uint32_t*)( &(frame_large->extended_length) );
    payload_size = ntohl(*size_l) | ((uint64_t)(ntohl(*size_h)) << 32) ;
  }
  else{
    //Otherwise: Small frame
    payload_size = frame_small->mask_n_length & WS_MASK_LENGTH;
  }
  return payload_size;
}

void* ws_frame_payload (void *buff){
  size_t header_size = ws_header_size(buff);
  return ((uint8_t*)buff) + header_size;
}

void ws_unmask_frame(void* buff, size_t len){
  struct ws_header_small *frame_small  = (struct ws_header_small*) buff;
  void *payload = ws_frame_payload(buff);

  //If not masked, return
  if( !(frame_small->mask_n_length & WS_FLAG_MASK) ){
    return;
  }

  //Otherwise, sanity checks and unmasking
  size_t   header_size  = ws_header_size(buff);
  uint64_t payload_size = ws_payload_size(buff);

  //Check buffer is big enough
  if(len < (header_size + payload_size)){
    //Buffer is not big enough for unmasking, exitting as-is
    logmsg(WS_LOG_WARNING, "Attempted to unmask a frame with a buffer size too low");
    return;
  }

  //All seems OK, unmask.
  //Pointer must be at the unmask key, meaning payload base - mask key len (4)
  void* unmask_buff = (uint8_t*)ws_frame_payload(buff) - WS_MASK_KEY_SIZE;
  ws_unmask_payload(unmask_buff, payload_size + WS_MASK_LENGTH);
  //DO NOT Clear the masked flag since we din't correct for mask key!
  //frame_small->mask_n_length = frame_small->mask_n_length & (~WS_FLAG_MASK);
}

uint32_t ws_mask_key(const void* buff){
  size_t header_size = ws_header_size(buff);
  void *mask_key_p = ((uint8_t*)buff) + header_size - WS_MASK_KEY_SIZE;
  return ntohl(*((uint32_t*)mask_key_p));
}

void ws_mask_payload(const void* header, void* payload, size_t len){
  uint32_t key = ws_mask_key(header);
  uint8_t  key_bytes[4] = {
    (key >> 24) & 0xFF,
    (key >> 16) & 0xFF,
    (key >>  8) & 0xFF,
    (key >>  0) & 0xFF,
  };
  int key_index = 0;
  uint8_t *buff_b = (uint8_t*)payload;
  while(len){
    *buff_b = *buff_b ^ key_bytes[key_index];

    len--;
    buff_b++;
    key_index = (key_index + 1) % 4;
  }
}

uint64_t ws_validate_frame(const void* buff, size_t len){
  size_t  header_size   = ws_header_size(buff);
  uint64_t payload_size = ws_payload_size(buff);

  //if len < (header_size + payload_size) then the frame is "not yet there"
  if(len < (header_size + payload_size)){
    return 0;
  }
  //Otherwise return the full frame size.
  else{
    return (header_size + payload_size);
  }
}
